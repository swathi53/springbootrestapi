package com.example.demo.controller;

import java.net.URI;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import com.example.demo.util.CustomErrorType;

@RestController
@RequestMapping("/api")
public class RestAPIController {

	public static final Logger logger = LoggerFactory.getLogger(RestAPIController.class);

	@Autowired
	UserService userService;

	/**
	 * Retrieve All Users
	 * 
	 * @return all users
	 */
	@RequestMapping(value = "/user/", method = RequestMethod.GET)
	public ResponseEntity<List<User>> listAllUsers() {
		List<User> users = userService.findAllUsers();
		if (users.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);

	}

	/**
	 * Retrieve Single User
	 * 
	 * @param id
	 * @return single user
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getUser(@PathVariable("id") long id) {
		logger.info("Fetching User with id {} ", id);
		User user = userService.findById(id);
		if (null == user) {
			logger.error("User with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("User with id " + id + " not found"), HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);

	}

	/**
	 * Create Users
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/user/", method = RequestMethod.POST)
	public ResponseEntity<?> createUsers(@RequestBody User user) {
		logger.info("Creating User: {}", user);
		if (userService.isUserExists(user)) {
			logger.error("Unable to create. A user with name {} already exists", user.getName());
			return new ResponseEntity(
					new CustomErrorType("Unable to create. A user with name " + user.getName() + " already exists."),
					HttpStatus.CONFLICT);
		}
		userService.saveUser(user);
		return new ResponseEntity<String>("SUCCESS", HttpStatus.CREATED);

	}

	/**
	 * Update Single User
	 * 
	 * @param id
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateUser(@PathVariable("id") long id, @RequestBody User user) {
		logger.info("Updating user with id {}", id);
		User currentUser = userService.findById(id);
		if (null == currentUser) {
			logger.error("Unable to update. User with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to update. User with id " + id + " not found"),
					HttpStatus.NOT_FOUND);
		}
		currentUser.setName(user.getName());
		currentUser.setAge(user.getAge());
		currentUser.setSalary(user.getSalary());
		userService.updateUser(currentUser);
		return new ResponseEntity<User>(currentUser, HttpStatus.OK);

	}

	/**
	 * Delete a User
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable("id") long id) {
		logger.info("Fetching and Deleting user with id {}", id);
		User user = userService.findById(id);
		if (null == user) {
			logger.error("Unable to Delete. User with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. User with id " + id + " not found"),
					HttpStatus.NOT_FOUND);
		}
		userService.deleteUserById(id);
		return new ResponseEntity<User>(HttpStatus.NO_CONTENT);

	}

	/**
	 * Delete All Users
	 * 
	 * @return
	 */
	@RequestMapping(value = "/user/", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteAllUser() {
		logger.info("Deleting all Users... ");
		userService.deleteAllUsers();
		return new ResponseEntity<User>(HttpStatus.NO_CONTENT);

	}

}
