# SpringBootRESTAPI

### User Management API
### Resourse version *1.0.0*
***

### Resourse Description

This API has below resources:

1. GET /api/user/{id} - getUser 
2. GET /api/user/ - listAllUsers
3. PUT /api/user/{id} 
4. POST /api/user/ - createUsers
5. DELETE /api/user/{id} - deleteUser
6. DELETE /api/user/ - deleteAllUser


# Swagger URL - http://localhost:8080/SpringBootRestApi/swagger-ui.html














